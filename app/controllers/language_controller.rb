class LanguageController < ApplicationController
  unloadable
  
  def index
    @languages = Language.all
  end
  
  def new
    @language = Language.new
  end

  def create
    @language = Language.create(language_params)
    if @language.save
      redirect_to action: :index
    else
      render :action => 'edit'
    end
  end
  
  def edit
    @language = Language.find(params[:id])
  end

  def update
    @language = Language.find(params[:id])
    @language.update_attributes(language_params)
    if @language.save
      redirect_to action: :index
    else
      render :action => 'edit'
    end
  end

  def destroy
    @language = Language.find(params[:id])
    @language.destroy
    redirect_to action: :index
  end
  
  private
  
  def language_params
    params.require(:language).permit(:name)
  end
end
