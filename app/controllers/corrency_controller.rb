class CorrencyController < ApplicationController
  unloadable

  def index
    @correncies = Corrency.all
  end
  
  def new
    @corrency = Corrency.new
  end

  def create
    @corrency = Corrency.create(corrency_params)    
    if @corrency.save
      redirect_to action: :index
    else
      render :action => 'edit'
    end
  end

  def edit
    @corrency = Corrency.find(params[:id])
  end

  def update
    @corrency = Corrency.find(params[:id])
    @corrency.update_attributes(corrency_params)
    if @corrency.save
      redirect_to action: :index
    else
      render :action => 'edit'
    end
  end

  def destroy
    @corrency = Corrency.find(params[:id])
    @corrency.destroy
    redirect_to action: :index
  end
  
  private
  
  def corrency_params
    params.require(:corrency).permit(:id,:name)
  end
end
