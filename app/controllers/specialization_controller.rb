class SpecializationController < ApplicationController
  unloadable

  def index
    @specializations = Specialization.all
  end
  
  def new
    @specialization = Specialization.new
  end

  def create
    @specialization = Specialization.create(specialization_params)
    if @specialization.save
      redirect_to action: :index
    else
      render :action => 'edit'
    end
  end

  def edit
    @specialization = Specialization.find(params[:id])
  end

  def update
    @specialization = Specialization.find(params[:id])
    @specialization.update_attributes(specialization_params)
    if @specialization.save
      redirect_to action: :index
    else
      render :action => 'edit'
    end
  end

  def destroy
    @specialization = Specialization.find(params[:id])
    @specialization.destroy
    redirect_to action: :index
  end
  
  private
  
  def specialization_params
    params.require(:specialization).permit(:name)
  end
end
