class UserPropertyController < ApplicationController
  unloadable

  def index
    @property = UserProperty.where(user_id: params[:user_id])
  end
  
  def show
    @user = User.where(id: params[:user_id]) 
  end
  
  def update
    id = params[:id]
    @new_specialization = params[:specialization_id]
    @old_specialization = UserProperty.where("user_id" =>id).pluck("specialization_id") 
    
    if @new_specialization != nil
       @new_specialization.each do |l|
        if !@old_specialization.include?(l.to_i)
          UserProperty.new("user_id" =>id,"specialization_id" =>l.to_i).save    
        end
      end
      @old_specialization.each do |l|
        if !@new_specialization.include?(l.to_s) && l != nil
          UserProperty.where("user_id" =>id,"specialization_id" =>l).first.destroy  
        end
      end
    end
    
    @new_language = params[:language_id]
    @old_language = UserProperty.where("user_id" =>id).pluck("language_id") 
    if @new_language != nil    
      #@new_language.map{|l| !@old_language.include?(l.to_i) || UserProperty.new("user_id" =>id,"language_id" =>l.to_i).save} 
      @new_language.each do |l|
        if !@old_language.include?(l.to_i)
          UserProperty.new("user_id" =>id,"language_id" =>l.to_i).save  
        end
      end
      @old_language.each do |l|
        if !@new_language.include?(l.to_s) && l != nil
          UserProperty.where("user_id" =>id,"language_id" =>l).first.destroy
        end
       end
    else
      
    end
     
    redirect_to edit_user_path(@user, :id=>id, :tab => 'property')
  end   
  
  private
  
   
end
