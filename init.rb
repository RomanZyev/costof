require 'redmine'
# require 'yaml_db' # create list language

Redmine::Plugin.register :costof do
  name 'Cost of plugin'
  author 'Zyev Roman'
  description 'This is a plugin for Redmine. For cost of issues'
  version '0.0.2'
  
  settings :default => {'empty' => true}, :partial => 'settings_costof'
  
end

# Hooks
require_dependency 'patches/hooks/view_hook'
require_relative 'lib/price_issues'
require_relative 'lib/users_helper_patch'
require_relative 'lib/phone_user'

Issue.send :include, ManuallyPriceIssue::PriceIssue
User.send :include, ManuallyUsers::UserPhone
UsersHelper.send :include, Patches::UsersHelperPatch
