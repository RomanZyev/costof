module RedmineIssueCostof
   class Hooks  < Redmine::Hook::ViewListener
       render_on :view_issues_form_details_bottom, :partial => 'issues/view_issues_form_details_bottom'
       render_on :view_issues_show_details_bottom, :partial => 'issues/view_issues_show_details_bottom'
       render_on :view_users_form, :partial => 'users/view_users_form'
       render_on :view_my_account, :partial => 'my/view_my_account'
        
   end
end