require_dependency 'issue'

module ManuallyPriceIssue
  module PriceIssue
    def self.included( base )
      base.class_eval do

       safe_attributes 'price',
              'corrency_id',
              'specialization_id',
              'language_id',
              :if => lambda {|issue, user| issue.attributes_editable?(user) }
                  
      end
    end
  end
end