require_dependency 'user'

module ManuallyUsers
  module UserPhone
    def self.included( base )
      base.class_eval do

       safe_attributes 'phone'
                  
      end
    end
  end
end