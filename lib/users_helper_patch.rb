require_dependency 'users_helper'

module Patches
  module UsersHelperPatch
    def self.included(base)
      base.send(:include, InstanceMethods)   
      
      base.class_eval do
          unloadable

          
          alias_method_chain :user_settings_tabs, :property   
        end
         
    end
   
   module InstanceMethods
      
     def user_settings_tabs_with_property
       tabs = [{:name => 'general', :partial => 'users/general', :label => :label_general},
            {:name => 'memberships', :partial => 'users/memberships', :label => :label_project_plural}
            ]
        if Group.givable.any?
          tabs.insert 1, {:name => 'groups', :partial => 'users/groups', :label => :label_group_plural}
        end
  
       tabs.insert tabs.size, {:name => 'property', :partial => 'users/property_show', :label => :label_user_properties}
       tabs
     end
     
    end
  end
end
