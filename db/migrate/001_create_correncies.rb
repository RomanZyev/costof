class CreateCorrencies < ActiveRecord::Migration
  def self.up
    create_table :correncies do |t|
      t.string :name
    end
    
    Corrency.create :name => "UAH", :id => 980
    Corrency.create :name => "EUR", :id => 978
    Corrency.create :name => "USD", :id => 840
   
  end
  def self.down
    drop_table :correncies
  end
end
