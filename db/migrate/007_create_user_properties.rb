class CreateUserProperties < ActiveRecord::Migration
  def self.up
    create_table :user_properties do |t|
      t.belongs_to  :user, index: true
      t.belongs_to  :language, index: true
      t.belongs_to  :specialization, index: true
    end
  end
  
  def self.down
    drop_table :user_properties
  end
end