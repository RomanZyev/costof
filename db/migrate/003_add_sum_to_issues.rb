class AddSumToIssues < ActiveRecord::Migration
  def self.up
    add_column :issues, :price, :integer, :default => 0
    add_column :issues, :corrency_id, :integer, :default => 980
 
  end

  def self.down
    remove_column :issues, :price
    remove_column :issues, :corrency_id

  end
end
