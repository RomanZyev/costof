class AddLanguageToIssues < ActiveRecord::Migration
  def self.up
    add_column :issues, :specialization_id, :integer
    add_column :issues, :language_id, :integer
  end

  def self.down
    remove_column :issues, :specialization_id
    remove_column :issues, :language_id
  end
end
